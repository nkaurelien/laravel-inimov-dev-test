-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour laravel
CREATE DATABASE IF NOT EXISTS `laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `laravel`;

-- Listage de la structure de la table laravel. activity_log
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) unsigned DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`),
  KEY `subject` (`subject_id`,`subject_type`),
  KEY `causer` (`causer_id`,`causer_type`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.activity_log : ~111 rows (environ)
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
REPLACE INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
	(1, 'default', 'created', 1, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(2, 'default', 'created', 1, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(3, 'default', 'created', 2, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(4, 'default', 'created', 3, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(5, 'default', 'created', 4, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(6, 'default', 'created', 5, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(7, 'default', 'created', 6, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(8, 'default', 'created', 7, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(9, 'default', 'created', 8, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:51', '2020-11-16 14:47:51'),
	(10, 'default', 'created', 9, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(11, 'default', 'created', 10, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(12, 'default', 'created', 2, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(13, 'default', 'created', 11, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(14, 'default', 'created', 12, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(15, 'default', 'created', 13, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(16, 'default', 'created', 14, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(17, 'default', 'created', 15, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(18, 'default', 'created', 16, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(19, 'default', 'created', 17, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(20, 'default', 'created', 18, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(21, 'default', 'created', 19, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(22, 'default', 'created', 20, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(23, 'default', 'created', 3, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(24, 'default', 'created', 21, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(25, 'default', 'created', 22, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:52', '2020-11-16 14:47:52'),
	(26, 'default', 'created', 23, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(27, 'default', 'created', 24, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(28, 'default', 'created', 25, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(29, 'default', 'created', 26, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(30, 'default', 'created', 27, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(31, 'default', 'created', 28, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(32, 'default', 'created', 29, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(33, 'default', 'created', 30, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(34, 'default', 'created', 4, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(35, 'default', 'created', 31, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(36, 'default', 'created', 32, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(37, 'default', 'created', 33, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(38, 'default', 'created', 34, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(39, 'default', 'created', 35, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(40, 'default', 'created', 36, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:53', '2020-11-16 14:47:53'),
	(41, 'default', 'created', 37, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(42, 'default', 'created', 38, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(43, 'default', 'created', 39, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(44, 'default', 'created', 40, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(45, 'default', 'created', 5, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(46, 'default', 'created', 41, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(47, 'default', 'created', 42, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(48, 'default', 'created', 43, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(49, 'default', 'created', 44, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(50, 'default', 'created', 45, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(51, 'default', 'created', 46, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(52, 'default', 'created', 47, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(53, 'default', 'created', 48, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(54, 'default', 'created', 49, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(55, 'default', 'created', 50, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(56, 'default', 'created', 6, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:54', '2020-11-16 14:47:54'),
	(57, 'default', 'created', 51, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(58, 'default', 'created', 52, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(59, 'default', 'created', 53, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(60, 'default', 'created', 54, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(61, 'default', 'created', 55, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(62, 'default', 'created', 56, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(63, 'default', 'created', 57, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(64, 'default', 'created', 58, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(65, 'default', 'created', 59, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(66, 'default', 'created', 60, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(67, 'default', 'created', 7, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(68, 'default', 'created', 61, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(69, 'default', 'created', 62, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(70, 'default', 'created', 63, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:55', '2020-11-16 14:47:55'),
	(71, 'default', 'created', 64, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(72, 'default', 'created', 65, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(73, 'default', 'created', 66, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(74, 'default', 'created', 67, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(75, 'default', 'created', 68, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(76, 'default', 'created', 69, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(77, 'default', 'created', 70, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(78, 'default', 'created', 8, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(79, 'default', 'created', 71, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(80, 'default', 'created', 72, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(81, 'default', 'created', 73, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(82, 'default', 'created', 74, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(83, 'default', 'created', 75, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(84, 'default', 'created', 76, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(85, 'default', 'created', 77, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:56', '2020-11-16 14:47:56'),
	(86, 'default', 'created', 78, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(87, 'default', 'created', 79, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(88, 'default', 'created', 80, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(89, 'default', 'created', 9, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(90, 'default', 'created', 81, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(91, 'default', 'created', 82, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(92, 'default', 'created', 83, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(93, 'default', 'created', 84, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(94, 'default', 'created', 85, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(95, 'default', 'created', 86, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(96, 'default', 'created', 87, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(97, 'default', 'created', 88, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(98, 'default', 'created', 89, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(99, 'default', 'created', 90, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:57', '2020-11-16 14:47:57'),
	(100, 'default', 'created', 10, 'App\\Models\\Category', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(101, 'default', 'created', 91, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(102, 'default', 'created', 92, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(103, 'default', 'created', 93, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(104, 'default', 'created', 94, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(105, 'default', 'created', 95, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(106, 'default', 'created', 96, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(107, 'default', 'created', 97, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(108, 'default', 'created', 98, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(109, 'default', 'created', 99, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(110, 'default', 'created', 100, 'App\\Models\\Product', NULL, NULL, '[]', '2020-11-16 14:47:58', '2020-11-16 14:47:58'),
	(111, 'default', 'created', 1, 'App\\Domains\\Announcement\\Models\\Announcement', NULL, NULL, '{"attributes": {"area": null, "type": "info", "enabled": true, "ends_at": null, "message": "This is a <strong>Global</strong> announcement that will show on both the frontend and backend. <em>See <strong>AnnouncementSeeder</strong> for more usage examples.</em>", "starts_at": null}}', '2020-11-16 14:47:59', '2020-11-16 14:47:59');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;

-- Listage de la structure de la table laravel. announcements
CREATE TABLE IF NOT EXISTS `announcements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` enum('frontend','backend') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('info','danger','warning','success') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `starts_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.announcements : ~1 rows (environ)
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
REPLACE INTO `announcements` (`id`, `area`, `type`, `message`, `enabled`, `starts_at`, `ends_at`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'info', 'This is a <strong>Global</strong> announcement that will show on both the frontend and backend. <em>See <strong>AnnouncementSeeder</strong> for more usage examples.</em>', 1, NULL, NULL, '2020-11-16 14:47:59', '2020-11-16 14:47:59');
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;

-- Listage de la structure de la table laravel. categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.categories : ~10 rows (environ)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
REPLACE INTO `categories` (`id`, `uid`, `slug`, `parent_id`, `name`, `level`, `image`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '78f6c74e-01af-40b8-87b3-e830f2009691', 'cat-81898', NULL, 'Cat 81898', 0, 'https://lorempixel.com/500/500/?84295', 1, '2009-03-30 16:32:44', '2020-11-16 14:47:51', NULL),
	(2, '9cc37353-e40d-4291-bfbb-f421bc9a1fe4', 'cat-32306', NULL, 'Cat 32306', 0, 'https://lorempixel.com/500/500/?79702', 1, '1974-06-11 02:33:49', '2020-11-16 14:47:52', NULL),
	(3, 'cec2f671-f867-4d39-8733-b6db650539ee', 'cat-4556', NULL, 'Cat 4556', 0, 'https://lorempixel.com/500/500/?37557', 1, '1986-07-30 08:18:49', '2020-11-16 14:47:52', NULL),
	(4, 'f9fc7d20-3afe-4095-96e2-b9f3f989b712', 'cat-46167', NULL, 'Cat 46167', 0, 'https://lorempixel.com/500/500/?75525', 1, '1988-09-24 06:42:46', '2020-11-16 14:47:53', NULL),
	(5, '02b8278a-3cd9-4870-b3e7-d780c1c32d4b', 'cat-96376', NULL, 'Cat 96376', 0, 'https://lorempixel.com/500/500/?74246', 1, '1975-05-07 17:48:41', '2020-11-16 14:47:54', NULL),
	(6, 'ff11df07-a7b1-4987-be81-998674e2b836', 'cat-91697', NULL, 'Cat 91697', 0, 'https://lorempixel.com/500/500/?79227', 1, '1990-06-13 21:15:17', '2020-11-16 14:47:54', NULL),
	(7, '217faa04-6476-42d6-a3d4-3c9a05671210', 'cat-7505', NULL, 'Cat 7505', 0, 'https://lorempixel.com/500/500/?40997', 1, '1993-12-12 18:20:26', '2020-11-16 14:47:55', NULL),
	(8, '8af8f875-6361-4b4a-9f01-80a3ef472ef5', 'cat-20200', NULL, 'Cat 20200', 0, 'https://lorempixel.com/500/500/?91955', 1, '2005-02-26 02:54:27', '2020-11-16 14:47:56', NULL),
	(9, 'd48d5fe6-1789-4731-b856-893cabc40f2b', 'cat-59257', NULL, 'Cat 59257', 0, 'https://lorempixel.com/500/500/?78405', 0, '1972-03-08 08:41:31', '2020-11-16 14:47:57', NULL),
	(10, 'beeebe2c-f62f-49a8-a901-cbde90ca6400', 'cat-97505', NULL, 'Cat 97505', 0, 'https://lorempixel.com/500/500/?79940', 1, '1978-01-25 21:46:45', '2020-11-16 14:47:57', NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Listage de la structure de la table laravel. failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.failed_jobs : ~0 rows (environ)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Listage de la structure de la table laravel. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.migrations : ~10 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2020_02_25_034148_create_permission_tables', 1),
	(5, '2020_05_25_021239_create_announcements_table', 1),
	(6, '2020_05_29_020244_create_password_histories_table', 1),
	(7, '2020_07_06_215139_create_activity_log_table', 1),
	(8, '2020_10_19_204342_create_two_factor_authentications_table', 1),
	(9, '2020_11_15_171903_create_products_table', 1),
	(10, '2020_11_15_171924_create_categories_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table laravel. model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.model_has_permissions : ~0 rows (environ)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Listage de la structure de la table laravel. model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.model_has_roles : ~1 rows (environ)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
REPLACE INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Domains\\Auth\\Models\\User', 1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Listage de la structure de la table laravel. password_histories
CREATE TABLE IF NOT EXISTS `password_histories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.password_histories : ~2 rows (environ)
/*!40000 ALTER TABLE `password_histories` DISABLE KEYS */;
REPLACE INTO `password_histories` (`id`, `model_type`, `model_id`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Domains\\Auth\\Models\\User', 1, '$2y$10$FU6dlKhtSSC4zKYTfA0NTuWVQqzmOjLUrpMG140yXDugM99jb7xk.', '2020-11-16 14:47:48', '2020-11-16 14:47:48'),
	(2, 'App\\Domains\\Auth\\Models\\User', 2, '$2y$10$BmMFPRoEI93g4u2F3o5MgO5LCts2PCf/ZyUc5vB1FJCOzHSbU3Uxu', '2020-11-16 14:47:48', '2020-11-16 14:47:48');
/*!40000 ALTER TABLE `password_histories` ENABLE KEYS */;

-- Listage de la structure de la table laravel. password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.password_resets : ~0 rows (environ)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Listage de la structure de la table laravel. permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_parent_id_foreign` (`parent_id`),
  CONSTRAINT `permissions_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.permissions : ~11 rows (environ)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
REPLACE INTO `permissions` (`id`, `type`, `guard_name`, `name`, `description`, `parent_id`, `sort`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'web', 'admin.access.user', 'All User Permissions', NULL, 1, '2020-11-16 14:47:48', '2020-11-16 14:47:48'),
	(2, 'admin', 'web', 'admin.access.user.list', 'View Users', 1, 1, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(3, 'admin', 'web', 'admin.access.user.deactivate', 'Deactivate Users', 1, 2, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(4, 'admin', 'web', 'admin.access.user.reactivate', 'Reactivate Users', 1, 3, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(5, 'admin', 'web', 'admin.access.user.clear-session', 'Clear User Sessions', 1, 4, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(6, 'admin', 'web', 'admin.access.user.impersonate', 'Impersonate Users', 1, 5, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(7, 'admin', 'web', 'admin.access.user.change-password', 'Change User Passwords', 1, 6, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(8, 'admin', 'web', 'admin.access.shopping', 'All Shopping Permissions', NULL, 1, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(9, 'admin', 'web', 'admin.access.shopping.list', 'View shopping categories & products', 8, 1, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(10, 'admin', 'web', 'admin.access.shopping.deactivate', 'Deactivate shopping categories & products', 8, 2, '2020-11-16 14:47:49', '2020-11-16 14:47:49'),
	(11, 'admin', 'web', 'admin.access.shopping.reactivate', 'Reactivate shopping categories & products', 8, 3, '2020-11-16 14:47:49', '2020-11-16 14:47:49');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Listage de la structure de la table laravel. products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '1',
  `price_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'XAF',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `type` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calendar` text COLLATE utf8mb4_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.products : ~100 rows (environ)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`id`, `category_id`, `uid`, `name`, `slug`, `detail`, `brand_name`, `brand_logo`, `tags`, `price`, `price_unit`, `quantity`, `type`, `image`, `calendar`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'cc22d9ac-1939-4e99-934e-6a152a7c9fc3', 'Art 80799', 'art-80799', 'Eligendi eveniet rerum minus cum. Quam exercitationem ut aut dolores. Dolor nobis odit temporibus qui fugiat consequuntur excepturi quia. Sint qui quia aut eum.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 846, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?66573', NULL, 1, '2019-11-29 02:40:24', '2020-11-16 14:47:51', NULL),
	(2, 1, 'aee78df4-47c4-4a44-bbd1-03b37de5e803', 'Art 97617', 'art-97617', 'Culpa nam magni sit ut. Nisi at molestias a recusandae. Perspiciatis nesciunt ipsa sit esse. Ut consequatur autem temporibus aut optio.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 753, 'USD', 1, 0, 'https://lorempixel.com/500/500/?14986', NULL, 0, '2012-12-15 20:11:25', '2020-11-16 14:47:51', NULL),
	(3, 1, '54883957-df6e-4436-86bb-ed9682e05486', 'Art 4269', 'art-4269', 'Et ratione doloribus modi sit. Atque libero iusto dicta tenetur minima veniam impedit similique.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 292, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?93178', NULL, 1, '1975-08-03 09:04:13', '2020-11-16 14:47:51', NULL),
	(4, 1, '4cb1ec51-7eea-4f35-b32a-745a63697f11', 'Art 71814', 'art-71814', 'Accusantium sint quaerat aut fugiat odio veritatis eos. Pariatur ipsum velit hic laudantium. Consequatur aliquid debitis reprehenderit non.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 580, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?99557', NULL, 1, '2001-07-20 04:17:58', '2020-11-16 14:47:51', NULL),
	(5, 1, 'b22769c8-1ff9-46bd-af71-446b73af0ea7', 'Art 31510', 'art-31510', 'Iste provident fugiat omnis. Et qui et nihil dolor quia nisi. Ullam impedit cupiditate perspiciatis voluptatum eos ut vitae consequatur. Qui aut quidem minima soluta corporis et ut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 781, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?82044', NULL, 1, '1975-07-01 02:24:48', '2020-11-16 14:47:51', NULL),
	(6, 1, '12de3634-e458-469a-9b34-33f182cae20d', 'Art 45180', 'art-45180', 'Earum consequatur veritatis a dolores et iusto. Non consequuntur placeat laboriosam possimus deserunt aut fuga. Rerum non voluptates dolores aut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 697, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?87038', NULL, 1, '1997-05-24 09:09:50', '2020-11-16 14:47:51', NULL),
	(7, 1, '7b4d9a2b-3898-4e55-8428-99ffe72ece45', 'Art 25522', 'art-25522', 'Molestiae excepturi hic velit in et. Id molestiae blanditiis eos debitis sit laborum. Quae temporibus dicta autem aperiam velit.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 222, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?18282', NULL, 1, '2013-12-05 00:31:44', '2020-11-16 14:47:51', NULL),
	(8, 1, 'c977fd84-07ae-41a4-8347-0fbb9628a968', 'Art 75479', 'art-75479', 'Error non culpa harum earum qui eaque ex. Similique id provident architecto nostrum dolor est dolor. Molestiae eum ut praesentium est ipsum. Labore quia ullam dolores expedita.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 920, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?57227', NULL, 1, '1975-06-18 10:34:18', '2020-11-16 14:47:51', NULL),
	(9, 1, '08b62586-937a-4e48-9e1d-5726ea3d4019', 'Art 41680', 'art-41680', 'Delectus vel quam eveniet tempora non distinctio facilis. Autem nihil incidunt maiores doloribus temporibus. Eos quas autem architecto et suscipit aut provident.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 888, 'USD', 1, 0, 'https://lorempixel.com/500/500/?14755', NULL, 0, '1972-08-13 04:37:47', '2020-11-16 14:47:52', NULL),
	(10, 1, 'bb894dcd-e503-4cde-bc70-b6a2cca190f0', 'Art 52300', 'art-52300', 'Recusandae tempore ab expedita vero rerum molestias. Non libero exercitationem voluptate deleniti ab consequuntur. Beatae omnis et quia laborum ipsa.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 251, 'USD', 1, 0, 'https://lorempixel.com/500/500/?68272', NULL, 1, '2003-06-04 21:22:31', '2020-11-16 14:47:52', NULL),
	(11, 2, '3b4cc6b0-9587-44a2-bbfc-6a9fe800e2ae', 'Art 52100', 'art-52100', 'Fugiat explicabo debitis facere. Aspernatur facilis consequuntur eveniet sed iste suscipit eaque. Est molestias quidem vel dolorem nostrum explicabo est.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 389, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?82312', NULL, 1, '1983-12-07 15:55:21', '2020-11-16 14:47:52', NULL),
	(12, 2, 'bd203e0e-3eff-4249-a6f5-2c0c6048f3e0', 'Art 768', 'art-768', 'Repudiandae est illo quasi laudantium et nostrum. Illum voluptatem aspernatur reiciendis totam. Explicabo porro excepturi nemo non ipsum nulla.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 28, 'USD', 1, 0, 'https://lorempixel.com/500/500/?15699', NULL, 1, '1980-05-31 02:58:12', '2020-11-16 14:47:52', NULL),
	(13, 2, '91d50751-f0eb-4d13-be1f-ad7ee6a5f4d9', 'Art 45371', 'art-45371', 'Laborum ducimus voluptates et ab placeat. Quo saepe ut similique facilis unde quod sint. Voluptas voluptatem magnam ipsa.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 580, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?25690', NULL, 1, '1974-07-05 17:14:47', '2020-11-16 14:47:52', NULL),
	(14, 2, '2dfbb604-5782-45dd-89b0-d6126a3fe62f', 'Art 42169', 'art-42169', 'Harum doloribus alias nemo. Aliquid et eius perferendis in quod. Ullam illum aliquid eveniet voluptate qui qui tempora. Aut voluptatibus aut laborum atque et voluptas modi ea.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 511, 'USD', 1, 0, 'https://lorempixel.com/500/500/?60637', NULL, 1, '1999-10-23 10:43:28', '2020-11-16 14:47:52', NULL),
	(15, 2, '90d0fcd3-59b1-453c-a04a-432373363659', 'Art 54606', 'art-54606', 'Aut vero quia quam est aut cumque. Consequatur illum quaerat molestiae veritatis asperiores ut. Ut culpa nostrum et. Beatae voluptatum quasi eos consequatur soluta id.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 271, 'USD', 1, 0, 'https://lorempixel.com/500/500/?73087', NULL, 1, '1995-01-04 17:09:06', '2020-11-16 14:47:52', NULL),
	(16, 2, 'ce011f62-5832-4b28-94e2-670d77856a98', 'Art 49704', 'art-49704', 'Est aut consectetur temporibus nisi. Aliquam quasi officia illo veritatis rerum. Recusandae quia corporis est voluptas. Hic et velit dolores.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 445, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?71930', NULL, 1, '1974-07-16 06:03:50', '2020-11-16 14:47:52', NULL),
	(17, 2, 'ffb62771-c310-4139-a070-57ee4828a7fd', 'Art 52161', 'art-52161', 'Voluptatibus et autem laudantium in autem magni. Asperiores nisi sint laboriosam possimus ea est. Molestiae voluptatum odit est voluptates vitae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 689, 'USD', 1, 0, 'https://lorempixel.com/500/500/?19870', NULL, 0, '1978-02-05 16:09:43', '2020-11-16 14:47:52', NULL),
	(18, 2, '30ef76d8-963a-4d84-93b1-434d1c8bface', 'Art 70776', 'art-70776', 'Voluptatem id veritatis vel explicabo. Ullam rem atque tempora. Ut eum minima quia nisi minus qui iusto.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 261, 'USD', 1, 0, 'https://lorempixel.com/500/500/?37745', NULL, 1, '1985-09-19 12:42:53', '2020-11-16 14:47:52', NULL),
	(19, 2, '300ea38e-31b7-41f5-bcbe-4f7bf68bd04e', 'Art 88757', 'art-88757', 'Molestiae corrupti aliquam aspernatur ratione. Assumenda quod quibusdam perspiciatis. Aperiam autem ut sit eum ipsum.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 709, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?19978', NULL, 1, '1981-08-10 05:26:25', '2020-11-16 14:47:52', NULL),
	(20, 2, 'e21a8810-dd83-40e7-bc0b-5e27bf375928', 'Art 58097', 'art-58097', 'Et facere quas nihil eligendi ducimus. Non quis dolorum sit beatae quam. Rerum deleniti quae ullam et aspernatur inventore.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 38, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?57375', NULL, 1, '1970-01-11 08:57:54', '2020-11-16 14:47:52', NULL),
	(21, 3, 'da8b9ae9-dc87-43fd-aa0c-f12b63ca8176', 'Art 2046', 'art-2046', 'Vel aperiam fugiat non recusandae voluptatem voluptatibus consequuntur nobis. Eligendi illo hic nostrum assumenda maxime voluptatum a. Velit aperiam atque eveniet. Quos illum totam ratione.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 250, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?37140', NULL, 1, '2014-07-14 12:43:46', '2020-11-16 14:47:52', NULL),
	(22, 3, '43123a12-4a06-448f-9a23-843041200421', 'Art 42846', 'art-42846', 'Voluptas ratione voluptate laboriosam at. Id exercitationem rem perspiciatis vel. Maiores sit tempore ut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 246, 'USD', 1, 0, 'https://lorempixel.com/500/500/?29535', NULL, 0, '2016-03-15 21:29:51', '2020-11-16 14:47:52', NULL),
	(23, 3, '893f0712-82af-47e2-9836-41e8800b99a3', 'Art 67394', 'art-67394', 'Doloremque reprehenderit temporibus dolores est. Officiis quia voluptates quam ut beatae rem quod. Consequatur ut repudiandae sed. Sit repellat ullam aliquam soluta.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 260, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?34642', NULL, 1, '1984-03-31 06:42:18', '2020-11-16 14:47:53', NULL),
	(24, 3, '20257e7a-70c1-4267-885c-c1c21af1316f', 'Art 10055', 'art-10055', 'Nulla aspernatur autem ea blanditiis quis mollitia. Sit eos tempora eveniet id eaque quis. Voluptas aut nam ratione eligendi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 208, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?79785', NULL, 1, '2017-02-03 08:00:05', '2020-11-16 14:47:53', NULL),
	(25, 3, '448ae5b1-0089-45a3-98e1-819c53e112a3', 'Art 66592', 'art-66592', 'Voluptatem voluptatem omnis nihil corporis ab facere itaque. Cum earum quae blanditiis eveniet ut numquam vitae. Odio quidem accusamus voluptate nulla et voluptates.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 242, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?87768', NULL, 1, '2013-07-21 00:20:07', '2020-11-16 14:47:53', NULL),
	(26, 3, '2d963037-beae-4868-ade3-1ec4f4f52010', 'Art 74418', 'art-74418', 'Omnis magnam sed incidunt neque aut qui nesciunt. Tenetur illum a vitae amet sit illum placeat ut. Voluptatum perspiciatis asperiores veniam mollitia.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 173, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?27390', NULL, 1, '2003-06-24 06:23:31', '2020-11-16 14:47:53', NULL),
	(27, 3, '41ef66af-9161-43f3-92f6-7a162bfbe305', 'Art 44745', 'art-44745', 'Omnis beatae magnam voluptates adipisci accusamus. Iste itaque non autem ut. Excepturi et alias qui aut excepturi qui.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 806, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?23637', NULL, 0, '2000-07-03 19:26:14', '2020-11-16 14:47:53', NULL),
	(28, 3, 'a9693fba-f15b-4f48-9800-3e765f236685', 'Art 60922', 'art-60922', 'Necessitatibus voluptatibus accusantium qui cumque et. Blanditiis qui explicabo commodi ratione quod. Et molestiae temporibus modi esse officia ut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 451, 'USD', 1, 0, 'https://lorempixel.com/500/500/?83917', NULL, 1, '1973-04-05 17:36:19', '2020-11-16 14:47:53', NULL),
	(29, 3, 'dfad3833-d66b-4341-88ca-c51975863647', 'Art 55600', 'art-55600', 'Optio et cum quas enim nisi. Harum quod reiciendis rerum dolores commodi qui. Maiores sit doloremque quidem.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 857, 'USD', 1, 0, 'https://lorempixel.com/500/500/?44184', NULL, 1, '2002-08-25 12:40:43', '2020-11-16 14:47:53', NULL),
	(30, 3, '5c323340-5c65-488d-8f63-002d49c1c3da', 'Art 16887', 'art-16887', 'In vel amet molestiae at id esse. Officia est vitae culpa tenetur aperiam iure. Velit qui officia consequatur ut eveniet.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 120, 'USD', 1, 0, 'https://lorempixel.com/500/500/?75178', NULL, 0, '2019-01-14 09:33:04', '2020-11-16 14:47:53', NULL),
	(31, 4, '320079ad-38b7-4d3a-b48a-c89d01991d7d', 'Art 15489', 'art-15489', 'Voluptatem dolores commodi consectetur commodi suscipit voluptas. Non necessitatibus voluptas voluptas in eaque. Ea aut fugiat minus hic et voluptas.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 190, 'USD', 1, 0, 'https://lorempixel.com/500/500/?89565', NULL, 1, '2006-08-08 05:01:31', '2020-11-16 14:47:53', NULL),
	(32, 4, '2527fb67-5bf6-469c-b5ad-78fac3e8671b', 'Art 24578', 'art-24578', 'Quaerat quaerat dolor et dolorum nobis fugiat qui. Nemo illo fugit quod temporibus. Quia deserunt eius odio explicabo facilis ut. Amet neque facere nulla deserunt nihil.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 305, 'USD', 1, 0, 'https://lorempixel.com/500/500/?33190', NULL, 1, '1978-01-06 09:55:13', '2020-11-16 14:47:53', NULL),
	(33, 4, '30175e3d-1ca0-4fb0-9707-69649128542f', 'Art 14412', 'art-14412', 'Ullam expedita quis distinctio est id cumque. Dolorem ea est natus eligendi. Labore voluptatem quis culpa labore nesciunt eaque. Rerum rerum dolorum sint ut tempore consectetur.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 312, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?79568', NULL, 1, '1988-12-13 11:41:41', '2020-11-16 14:47:53', NULL),
	(34, 4, '4ca17ca2-5da1-40da-bc50-fe87de02a288', 'Art 71927', 'art-71927', 'Beatae perspiciatis expedita enim ut magni voluptas ea. Eum aut et non sed eaque. Dolorem illum vel nisi ut recusandae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 491, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?34423', NULL, 1, '1989-06-30 01:42:41', '2020-11-16 14:47:53', NULL),
	(35, 4, 'a8350fa6-110c-4c5e-8b68-6770c2ca6987', 'Art 2045', 'art-2045', 'Aliquam voluptates nam magni aut accusamus. Est sed qui sit id laboriosam quia harum quo. Ut maiores aut non error iure. Sunt maxime quasi adipisci veniam ducimus alias soluta voluptatibus.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 322, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?60762', NULL, 1, '2006-06-20 15:28:32', '2020-11-16 14:47:53', NULL),
	(36, 4, 'f2039474-0eab-48e0-bd03-41db1563734c', 'Art 57288', 'art-57288', 'Inventore labore debitis et perferendis autem. Deleniti consequatur enim voluptate aut reiciendis hic officia. Ea molestias voluptas in qui.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 954, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?22412', NULL, 1, '2011-08-28 00:04:47', '2020-11-16 14:47:53', NULL),
	(37, 4, '78a298dc-82b2-4a52-8ddc-84864fabd7e0', 'Art 44634', 'art-44634', 'Quas autem perspiciatis vel sint. Doloribus inventore necessitatibus amet quos assumenda. Rerum explicabo nobis quibusdam libero voluptatem et. Eveniet harum qui modi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 121, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?10394', NULL, 1, '1984-07-03 01:40:08', '2020-11-16 14:47:53', NULL),
	(38, 4, '466e6e8d-8baf-434f-a287-d43927f2ad3d', 'Art 40710', 'art-40710', 'Voluptas sit quam consequuntur velit cum. Non quam modi cumque nihil saepe rem id aut. Eos assumenda eligendi doloribus possimus quia quibusdam aut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 43, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?20215', NULL, 1, '1995-01-13 16:27:29', '2020-11-16 14:47:54', NULL),
	(39, 4, '0ac125cb-504b-4385-a033-f307c32766aa', 'Art 99371', 'art-99371', 'Voluptatem eveniet minus natus similique porro dolorum. Beatae nisi vel et ut sit qui. Hic est sunt et eveniet et inventore.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 146, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?23475', NULL, 0, '2010-08-03 11:22:17', '2020-11-16 14:47:54', NULL),
	(40, 4, '1636a1ce-7a9a-4a3c-98b9-b689fee0d144', 'Art 77586', 'art-77586', 'Quia quis natus alias eos occaecati. Quia voluptatum doloribus nihil quidem illum dolores voluptas architecto. Error consequuntur voluptatum consequuntur est laudantium qui incidunt.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 460, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?87328', NULL, 0, '1970-05-12 06:59:46', '2020-11-16 14:47:54', NULL),
	(41, 5, '27a4745b-be6b-4c22-877b-2c42f120ca40', 'Art 95431', 'art-95431', 'Quia sed non consequatur nihil est. Repudiandae quo error cum sapiente qui. Sequi sit autem harum molestias. Et veritatis sapiente distinctio ad.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 516, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?32373', NULL, 0, '1986-11-22 21:01:28', '2020-11-16 14:47:54', NULL),
	(42, 5, '7657a201-b758-49ec-b747-d8cb78eee6f4', 'Art 62528', 'art-62528', 'Pariatur labore inventore dicta voluptate in. Mollitia deleniti vero repudiandae ut velit et eligendi qui.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 234, 'USD', 1, 0, 'https://lorempixel.com/500/500/?62287', NULL, 1, '2004-12-14 13:00:22', '2020-11-16 14:47:54', NULL),
	(43, 5, '3341d429-88e8-442b-9fdd-6c3bda5c4e67', 'Art 21075', 'art-21075', 'Optio accusantium qui facilis adipisci quam et. Sint doloremque porro ad sint et. Cupiditate rem consequatur similique nihil deserunt id. Dolor aperiam illo non optio quia est assumenda.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 970, 'USD', 1, 0, 'https://lorempixel.com/500/500/?57867', NULL, 1, '1991-05-27 02:43:19', '2020-11-16 14:47:54', NULL),
	(44, 5, '3a0434b8-8467-4395-a80c-a10d1a186d5b', 'Art 99538', 'art-99538', 'Tenetur dolores blanditiis dolorem omnis. Recusandae explicabo illum rerum assumenda quis. Inventore necessitatibus sapiente tenetur voluptas doloremque eligendi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 962, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?23607', NULL, 1, '2018-06-07 14:04:04', '2020-11-16 14:47:54', NULL),
	(45, 5, '4bfb426b-f11c-4315-a71b-adf52b72dc8b', 'Art 63652', 'art-63652', 'Laudantium quis ut quia omnis et ut mollitia. Quia quam reprehenderit consequuntur odit nostrum.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 430, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?34460', NULL, 1, '2003-11-15 21:39:47', '2020-11-16 14:47:54', NULL),
	(46, 5, '49e17d2e-fc93-4893-ae54-c782b55d9ab0', 'Art 2769', 'art-2769', 'Quaerat dolores nobis ut. Consequatur laudantium itaque ut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 323, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?40603', NULL, 1, '2000-09-14 03:02:39', '2020-11-16 14:47:54', NULL),
	(47, 5, 'ffb480ed-55af-41b5-b458-8c7d7c31d066', 'Art 11381', 'art-11381', 'At reprehenderit eum dolor consequuntur repellendus quas nesciunt id. Repellat quod totam eos occaecati.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 179, 'USD', 1, 0, 'https://lorempixel.com/500/500/?45771', NULL, 1, '2014-11-02 19:38:56', '2020-11-16 14:47:54', NULL),
	(48, 5, 'c7f5bda9-ce42-4dc2-8a24-6bb06fd233eb', 'Art 37055', 'art-37055', 'Neque voluptas et ab ea voluptate. Quisquam nihil eos quo consectetur deserunt est doloribus.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 204, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?96662', NULL, 1, '1999-08-04 18:25:40', '2020-11-16 14:47:54', NULL),
	(49, 5, '4caa1c1f-7bfb-4c6e-88e1-c604d6a383a6', 'Art 91838', 'art-91838', 'Non voluptatem qui beatae veritatis qui. Non est dolorem ab. Eum ut est dolorem repellendus voluptas omnis. Ut autem sapiente vel cum nemo quisquam eaque consequuntur.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 803, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?72882', NULL, 1, '1976-02-25 08:18:53', '2020-11-16 14:47:54', NULL),
	(50, 5, 'b148634b-96c6-4742-9d58-f6d516a9c9dc', 'Art 12909', 'art-12909', 'Totam ad ut dicta. Praesentium eaque ad ducimus molestias sint fugiat. Voluptas rerum vel qui et. Autem labore consequatur quia sit officiis.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 755, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?87632', NULL, 1, '1975-09-18 05:28:04', '2020-11-16 14:47:54', NULL),
	(51, 6, 'b189eb74-dd4f-4df2-afdb-262228aaf365', 'Art 49640', 'art-49640', 'Quam sed veritatis ut officiis enim. Esse explicabo nihil vero molestias molestiae voluptate modi. Consequatur dolor minima provident quam blanditiis.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 151, 'USD', 1, 0, 'https://lorempixel.com/500/500/?52475', NULL, 1, '1977-09-22 12:24:52', '2020-11-16 14:47:55', NULL),
	(52, 6, '8d4cc865-3cd0-4287-bb77-d4fb75a32278', 'Art 52243', 'art-52243', 'Nobis doloribus aspernatur et consequatur nisi rerum corporis. Quo consequatur quibusdam aut explicabo. Culpa dolorem hic quos reprehenderit aut et dicta assumenda.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 311, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?88621', NULL, 1, '1983-04-14 19:55:59', '2020-11-16 14:47:55', NULL),
	(53, 6, '790334d3-18c6-4a5f-83a3-fc8959ab4413', 'Art 44478', 'art-44478', 'Voluptas a nisi et perferendis eum architecto. Nobis minima pariatur maxime animi fugit veniam molestiae aut. Aut id animi quod consequatur occaecati consequatur.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 670, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?98926', NULL, 1, '1988-09-18 13:18:55', '2020-11-16 14:47:55', NULL),
	(54, 6, '8eb6dc33-1edd-4216-952b-8e0225a03723', 'Art 71939', 'art-71939', 'Nemo aperiam architecto quas eum sapiente. Aut sunt dolore dolorem harum eveniet. Autem est aut est ipsum.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 315, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?77661', NULL, 1, '1979-02-20 18:59:48', '2020-11-16 14:47:55', NULL),
	(55, 6, 'd6df52fc-9472-44bd-ae7e-2117c7d9c4b4', 'Art 72922', 'art-72922', 'Nesciunt sit et debitis fuga. Qui voluptates aliquam autem quo consequuntur odit nemo.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 996, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?14645', NULL, 1, '1973-11-17 20:58:57', '2020-11-16 14:47:55', NULL),
	(56, 6, '0e62a1d8-6bc2-4d38-8a00-bc0c68324600', 'Art 30665', 'art-30665', 'Sit molestiae voluptatum dolores beatae fuga quod. Consectetur totam ut fuga repudiandae quibusdam. Alias odit fuga esse et enim omnis itaque.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 716, 'USD', 1, 0, 'https://lorempixel.com/500/500/?88316', NULL, 1, '1999-07-02 15:38:03', '2020-11-16 14:47:55', NULL),
	(57, 6, '2b7d43e4-a42d-4bac-b720-c8feb6c48070', 'Art 49837', 'art-49837', 'Unde laudantium ab illo vitae tempora. Voluptates aut atque non dignissimos id rerum. Cum error culpa maiores ut et voluptatum veritatis.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 529, 'USD', 1, 0, 'https://lorempixel.com/500/500/?65205', NULL, 1, '1998-08-07 18:29:52', '2020-11-16 14:47:55', NULL),
	(58, 6, 'bb758790-166c-4447-9a17-d83a19817d5a', 'Art 2849', 'art-2849', 'Rerum illum perspiciatis maiores nesciunt aperiam beatae sapiente delectus. Consequatur saepe culpa ducimus dolor qui.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 239, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?53302', NULL, 1, '2011-05-11 17:36:17', '2020-11-16 14:47:55', NULL),
	(59, 6, 'c6f68c9c-5021-417e-9a91-60c17a289260', 'Art 50749', 'art-50749', 'Ut odit ab voluptatem accusantium quis incidunt. Sunt distinctio illum quidem voluptatem eveniet. Ad eligendi dolor voluptas voluptas qui dolore. Ullam laborum sapiente officiis tenetur sed.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 500, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?81761', NULL, 0, '1993-09-22 09:22:23', '2020-11-16 14:47:55', NULL),
	(60, 6, '470e7914-b1f3-48b4-ab92-f324c09f12ad', 'Art 15259', 'art-15259', 'Amet esse libero expedita. Placeat quas impedit quae reprehenderit est autem. Eveniet voluptate dolor dolorem et consequatur facilis eveniet.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 108, 'USD', 1, 0, 'https://lorempixel.com/500/500/?80063', NULL, 1, '2019-10-21 06:29:57', '2020-11-16 14:47:55', NULL),
	(61, 7, '701cf8da-24d4-4a7c-babd-0d92437af176', 'Art 52220', 'art-52220', 'Quam voluptatem rem aperiam fugit. Et non rerum reiciendis ut. Quod nam vel eveniet. Asperiores non a voluptatibus.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 223, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?32970', NULL, 1, '1993-04-17 23:29:29', '2020-11-16 14:47:55', NULL),
	(62, 7, 'dd2091de-0e51-40a4-9cf6-fe061e6e318b', 'Art 23836', 'art-23836', 'Aut quia voluptates nostrum quo ullam. Explicabo et est odio quia quia ab laborum voluptatem. Non rerum id voluptatem sint quia tempora eveniet. Perspiciatis voluptatem fuga qui numquam in.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 865, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?31767', NULL, 1, '2011-08-11 17:45:38', '2020-11-16 14:47:55', NULL),
	(63, 7, '0100e802-eeb5-40fc-a637-8e78a733d068', 'Art 86940', 'art-86940', 'Quam illo ea fugit magnam asperiores quod nulla. Quasi ipsam natus maiores in et. Recusandae aut est delectus ex amet ullam eos in. Architecto laudantium temporibus est possimus enim beatae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 177, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?82787', NULL, 1, '2019-02-24 21:29:30', '2020-11-16 14:47:55', NULL),
	(64, 7, '30452387-e4fb-4046-928a-78d51d75590a', 'Art 48885', 'art-48885', 'Omnis rerum natus earum quibusdam laborum facilis et. Sapiente quasi qui qui deleniti asperiores blanditiis asperiores. Nostrum magni placeat facilis non minus eveniet voluptatem.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 322, 'USD', 1, 0, 'https://lorempixel.com/500/500/?31967', NULL, 0, '2003-03-21 13:44:46', '2020-11-16 14:47:56', NULL),
	(65, 7, 'ed36c31e-7293-4a8a-a9e9-6c5662eb672f', 'Art 97680', 'art-97680', 'Tenetur placeat voluptatem id reprehenderit asperiores. Tempora impedit nemo blanditiis asperiores saepe expedita.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 443, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?52156', NULL, 1, '1983-10-11 05:31:46', '2020-11-16 14:47:56', NULL),
	(66, 7, 'b1c0635f-2d67-4a42-8c25-181dc6e4f728', 'Art 89936', 'art-89936', 'Aliquid dolore necessitatibus nemo tenetur reiciendis assumenda. Aut totam sed ut amet quia. Quo natus sint voluptate.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 833, 'USD', 1, 0, 'https://lorempixel.com/500/500/?42454', NULL, 1, '1988-12-25 07:50:48', '2020-11-16 14:47:56', NULL),
	(67, 7, '8ce32480-c1dc-470b-a7ad-dbcde9887f76', 'Art 22408', 'art-22408', 'Voluptatum et recusandae delectus. Quae voluptatem velit consequuntur. Quisquam facilis qui porro est commodi corporis accusamus odit. Porro rem praesentium quia dolorem.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 586, 'USD', 1, 0, 'https://lorempixel.com/500/500/?73812', NULL, 1, '1997-03-17 14:07:39', '2020-11-16 14:47:56', NULL),
	(68, 7, 'a337c1fb-fdec-4421-bf8e-14dd2f0d54cf', 'Art 43838', 'art-43838', 'Voluptas at est similique aperiam rerum nihil. Nemo et omnis eligendi quibusdam eligendi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 533, 'USD', 1, 0, 'https://lorempixel.com/500/500/?94095', NULL, 1, '1983-12-23 02:05:14', '2020-11-16 14:47:56', NULL),
	(69, 7, '35b6052e-4510-4330-84ea-ec3f1026b90a', 'Art 22668', 'art-22668', 'Sint autem esse eius in necessitatibus. Reiciendis enim suscipit minus sint. Tenetur et tempore nulla quisquam aut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 706, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?97660', NULL, 1, '2012-04-26 05:15:47', '2020-11-16 14:47:56', NULL),
	(70, 7, '27cf025e-80b9-419a-b658-aafe091c08e9', 'Art 25151', 'art-25151', 'Officia ipsam aliquam dolorem voluptatem rerum. Dolores cum ducimus fuga accusamus atque beatae voluptas. Esse asperiores dolorem ipsum voluptas adipisci reprehenderit quia.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 901, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?97514', NULL, 1, '1971-11-06 18:07:11', '2020-11-16 14:47:56', NULL),
	(71, 8, '64b665e3-d7f8-4b01-8bf9-116bc67639e6', 'Art 23082', 'art-23082', 'Qui excepturi labore sed distinctio vitae. Asperiores sed dolor praesentium explicabo distinctio et et sit. Et ipsa minus culpa voluptatibus quia eveniet dolore. Ipsum magnam placeat fugit nam.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 846, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?50185', NULL, 0, '1983-06-18 12:24:00', '2020-11-16 14:47:56', NULL),
	(72, 8, 'a9292d9a-cf38-41c2-bcf0-94ac8c09cca1', 'Art 22846', 'art-22846', 'Voluptas et in repellat error sit. Deleniti et id quibusdam autem. Reiciendis molestiae aspernatur illum in.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 909, 'USD', 1, 0, 'https://lorempixel.com/500/500/?26206', NULL, 1, '2005-07-03 06:00:39', '2020-11-16 14:47:56', NULL),
	(73, 8, '7ff7ef00-bceb-455c-ac10-1b1f20d6901f', 'Art 75009', 'art-75009', 'Repudiandae sit itaque consectetur sequi aliquam et eaque est. Enim accusantium et velit voluptates vitae. Neque omnis id dolor numquam.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 10, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?78328', NULL, 1, '1972-06-09 21:37:35', '2020-11-16 14:47:56', NULL),
	(74, 8, 'b07b196c-3ebc-47da-865b-4ac52a2f8bf7', 'Art 10010', 'art-10010', 'Ut animi iste quam. Ipsa praesentium voluptas repellendus quas. Ut est praesentium eveniet modi reprehenderit odio sed. Similique velit dolores est eligendi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 765, 'USD', 1, 0, 'https://lorempixel.com/500/500/?19825', NULL, 1, '1989-12-27 19:48:29', '2020-11-16 14:47:56', NULL),
	(75, 8, 'b5e21b92-a905-4572-906e-320e6948cf0d', 'Art 84642', 'art-84642', 'Amet assumenda fugiat tempore asperiores et quo. Et sed eum assumenda. Velit neque quasi excepturi inventore et rerum aut nemo.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 657, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?41884', NULL, 1, '1989-08-03 13:42:01', '2020-11-16 14:47:56', NULL),
	(76, 8, 'f08c9d05-433f-4610-bdc0-1f6915b5b721', 'Art 93760', 'art-93760', 'Quae quam unde voluptatem corporis. Cum aut est corrupti facere architecto dicta. Maiores iure qui sunt repellat voluptas cupiditate sed. Dolorem laborum molestias error harum.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 59, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?20053', NULL, 1, '1994-07-27 10:53:16', '2020-11-16 14:47:56', NULL),
	(77, 8, 'a9be6314-0def-488d-a997-421f9afd6c51', 'Art 25209', 'art-25209', 'Debitis hic reprehenderit voluptatem enim iusto. Exercitationem consequatur qui excepturi et. Non officiis atque eum et magni doloremque.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 822, 'USD', 1, 0, 'https://lorempixel.com/500/500/?17443', NULL, 0, '1988-06-08 08:41:49', '2020-11-16 14:47:56', NULL),
	(78, 8, '69d0478d-917d-4983-9820-0e3e468ca9fd', 'Art 55968', 'art-55968', 'Hic labore eveniet aut sed doloremque ea. Quia non sit hic possimus eos ad.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 47, 'USD', 1, 0, 'https://lorempixel.com/500/500/?88437', NULL, 1, '1982-01-20 20:48:20', '2020-11-16 14:47:57', NULL),
	(79, 8, '4c853962-4c2e-4648-bdc0-6f5a635a1dfb', 'Art 23958', 'art-23958', 'Optio numquam rerum voluptatum voluptatem. Facere ducimus nobis quos praesentium. Recusandae numquam totam sed quia. Id quia architecto aliquid dolor.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 626, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?61820', NULL, 1, '2009-04-28 16:17:16', '2020-11-16 14:47:57', NULL),
	(80, 8, 'd4412381-6513-4559-ade1-d04acb260a89', 'Art 16721', 'art-16721', 'Sed veritatis laboriosam in quod voluptatem sunt. Mollitia consequatur a tempore. Delectus ut aut quod non dicta. Cum nam omnis et non aut ipsa.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 520, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?42862', NULL, 1, '2006-10-13 00:07:15', '2020-11-16 14:47:57', NULL),
	(81, 9, 'a8cb63b5-5ead-488c-bc4a-81db5392bbca', 'Art 33320', 'art-33320', 'Pariatur aut omnis officia cumque accusantium nemo perspiciatis. Ut ea necessitatibus enim et rerum cum voluptatum. A distinctio possimus sapiente doloremque enim neque. Vero cum sint nihil at autem.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 510, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?25151', NULL, 1, '2004-10-30 15:04:23', '2020-11-16 14:47:57', NULL),
	(82, 9, '87278253-8244-47cc-a404-6d6ef029fbc6', 'Art 33040', 'art-33040', 'Quia autem iusto qui sed quasi repellendus ut. Nisi est quia in error. Aut non a velit totam.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 254, 'USD', 1, 0, 'https://lorempixel.com/500/500/?60366', NULL, 1, '2011-09-14 06:31:04', '2020-11-16 14:47:57', NULL),
	(83, 9, '8e71b4a8-c31a-4ff7-84d6-ed8516e442d4', 'Art 63662', 'art-63662', 'Dolorum aut laborum rem ab saepe temporibus aut. Quos fugiat alias exercitationem quod. Unde amet iste aliquam. Culpa ut quos nesciunt et minima modi molestiae voluptatem.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 946, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?29662', NULL, 0, '1998-12-06 20:59:49', '2020-11-16 14:47:57', NULL),
	(84, 9, 'e59aaf7d-9322-4db2-8ce1-10e998fd4797', 'Art 97119', 'art-97119', 'Sunt illum dignissimos eligendi et omnis dolor et saepe. Beatae ut molestiae nam eligendi officia dolores et. Pariatur et dolore quia sit vero. Consequuntur cumque nostrum reprehenderit.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 430, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?11632', NULL, 1, '2008-06-03 00:48:36', '2020-11-16 14:47:57', NULL),
	(85, 9, '243207e2-9592-47cb-b6bf-8013333ff136', 'Art 52979', 'art-52979', 'Perspiciatis qui ducimus maxime sit nihil quis qui voluptatem. Quidem sint dolor ut cum. Eum quos explicabo est aliquam ipsum aut autem. Dolor facere rem provident eaque.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 175, 'USD', 1, 0, 'https://lorempixel.com/500/500/?62195', NULL, 1, '2008-04-06 23:56:17', '2020-11-16 14:47:57', NULL),
	(86, 9, 'd4cb9820-be82-4db6-bc9b-fba06e1f142e', 'Art 92891', 'art-92891', 'Fugit ad voluptatum qui et. Amet qui saepe laudantium ab. Necessitatibus et accusamus libero eius cumque dolores. Aspernatur qui molestiae saepe beatae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 871, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?75213', NULL, 1, '1980-10-04 12:59:01', '2020-11-16 14:47:57', NULL),
	(87, 9, '9c737a75-4da5-41c1-8873-4e3eb9b16ee4', 'Art 13840', 'art-13840', 'Nobis recusandae eum perferendis assumenda. Sapiente tempore aut facere quo in aut natus. Et veritatis quia amet adipisci. Omnis in dolorum culpa id illo culpa.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 900, 'USD', 1, 0, 'https://lorempixel.com/500/500/?26942', NULL, 1, '1999-06-07 07:31:43', '2020-11-16 14:47:57', NULL),
	(88, 9, 'cb9fc628-71ba-4dd7-af13-91b86a50f867', 'Art 6273', 'art-6273', 'Quas veritatis enim nihil. Eius quo enim corporis et. Rerum doloremque dignissimos aut optio nemo.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 178, 'USD', 1, 0, 'https://lorempixel.com/500/500/?79191', NULL, 1, '2019-09-08 11:21:46', '2020-11-16 14:47:57', NULL),
	(89, 9, 'c09ef21c-f61c-4da1-86bd-4ada74513872', 'Art 92675', 'art-92675', 'Voluptatum aut cum ut dolore. Dolorum aperiam minus ut a illo autem beatae. Quod ut voluptas temporibus corporis incidunt error molestiae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 922, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?74201', NULL, 0, '1971-09-24 20:10:12', '2020-11-16 14:47:57', NULL),
	(90, 9, 'e3a9fd2a-41a1-4135-a259-a6f3f23eb773', 'Art 13731', 'art-13731', 'Soluta quidem numquam voluptatem quidem. Beatae dolore reprehenderit aut voluptas qui laudantium recusandae.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 751, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?65246', NULL, 1, '2011-07-24 20:15:58', '2020-11-16 14:47:57', NULL),
	(91, 10, 'a91b0b6b-ced1-4dd1-b151-c6d645d5eb7b', 'Art 86946', 'art-86946', 'Dolor incidunt impedit velit in reiciendis iusto eius. Voluptatibus rerum nostrum provident possimus. Quia et nihil quo magnam ducimus ratione. Maxime soluta quibusdam adipisci neque ea.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 951, 'USD', 1, 0, 'https://lorempixel.com/500/500/?73281', NULL, 1, '2012-08-29 17:43:04', '2020-11-16 14:47:58', NULL),
	(92, 10, '3f5f0f7e-815c-4581-b3b8-b9ad72af8ee6', 'Art 38300', 'art-38300', 'Animi quae fugiat quibusdam nihil impedit placeat ea blanditiis. Qui et et saepe est quis.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 355, 'USD', 1, 0, 'https://lorempixel.com/500/500/?67217', NULL, 1, '2004-06-15 05:21:38', '2020-11-16 14:47:58', NULL),
	(93, 10, '85c0c5a9-1552-4b1f-ac5d-d60042b24099', 'Art 50664', 'art-50664', 'Fugiat magnam labore similique. Accusamus et labore sed qui qui officia pariatur ut.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 89, 'USD', 1, 0, 'https://lorempixel.com/500/500/?70929', NULL, 1, '1989-12-01 16:28:00', '2020-11-16 14:47:58', NULL),
	(94, 10, '82bd2ee1-39db-4fec-8ea4-524d3232ee4a', 'Art 38808', 'art-38808', 'Voluptatem et et at quasi. Impedit distinctio molestias adipisci commodi tempora. Tempora itaque autem fuga autem laboriosam qui. Dolorum sit odit quia perspiciatis maiores.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 723, 'USD', 1, 0, 'https://lorempixel.com/500/500/?44103', NULL, 1, '1990-05-21 15:23:44', '2020-11-16 14:47:58', NULL),
	(95, 10, '36c0de73-41ca-4671-93b7-7d54089908ae', 'Art 6678', 'art-6678', 'Qui voluptatum officiis ea odio. Facilis dolore est doloremque eligendi explicabo ut quasi. Quia cumque tempora natus tempora iure iure est. Qui ut pariatur ducimus.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 469, 'XAF', 1, 0, 'https://lorempixel.com/500/500/?81036', NULL, 1, '2015-01-27 21:14:48', '2020-11-16 14:47:58', NULL),
	(96, 10, '87ec0e33-4092-473e-9956-d3f9f8ca077a', 'Art 46660', 'art-46660', 'Dolorem veniam dignissimos debitis excepturi et. Vel est sequi laboriosam quia. Voluptas qui laudantium tempora qui voluptatum ex animi.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 334, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?99593', NULL, 0, '1973-02-14 02:52:50', '2020-11-16 14:47:58', NULL),
	(97, 10, '8453b209-2fcb-42e0-9f40-4ed4473a0fb5', 'Art 30321', 'art-30321', 'Iste rerum reprehenderit quod ex laudantium nemo. Libero officia ad omnis culpa laudantium quia. In est dolore iure eos voluptatem mollitia nulla.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 784, 'USD', 1, 0, 'https://lorempixel.com/500/500/?59572', NULL, 0, '1978-09-12 00:11:36', '2020-11-16 14:47:58', NULL),
	(98, 10, '541c0e12-380b-4e2f-9b02-81a96899519a', 'Art 62420', 'art-62420', 'Quibusdam ipsam sit ullam ratione laudantium enim facere. Nihil porro maxime reprehenderit vero occaecati labore suscipit deserunt. Eum harum nulla facilis. Et ipsam facilis eos natus.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 930, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?16748', NULL, 0, '1973-04-10 16:52:52', '2020-11-16 14:47:58', NULL),
	(99, 10, '49924738-43c8-44f7-b823-c88f167f62e1', 'Art 11831', 'art-11831', 'Error facere reprehenderit minima voluptate dignissimos nihil. Earum ut dolorem ipsum quam perspiciatis. Sed ea corrupti fugiat. Laborum sed repellendus sunt vero sunt mollitia qui.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 663, 'USD', 1, 0, 'https://lorempixel.com/500/500/?26351', NULL, 1, '1990-03-31 11:14:52', '2020-11-16 14:47:58', NULL),
	(100, 10, '49766009-fcdf-407b-9bed-03d69be25ff2', 'Art 81902', 'art-81902', 'Impedit et qui quam animi ut est aspernatur. Tempora quisquam hic dolorum atque autem voluptas adipisci. Laborum alias ut qui dolor et enim.', 'inimov', 'https://www.inimov.com/images/logo-inimov-blanc.png', 'tag1, tag2, tag3, tag4', 530, 'EUR', 1, 0, 'https://lorempixel.com/500/500/?15228', NULL, 1, '2001-03-22 02:24:13', '2020-11-16 14:47:58', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Listage de la structure de la table laravel. roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','manager','user') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.roles : ~2 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `type`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', 'web', '2020-11-16 14:47:48', '2020-11-16 14:47:48'),
	(2, 'manager', 'Manager', 'web', '2020-11-16 14:47:49', '2020-11-16 14:47:49');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Listage de la structure de la table laravel. role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.role_has_permissions : ~0 rows (environ)
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Listage de la structure de la table laravel. two_factor_authentications
CREATE TABLE IF NOT EXISTS `two_factor_authentications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authenticatable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticatable_id` bigint(20) unsigned NOT NULL,
  `shared_secret` blob NOT NULL,
  `enabled_at` timestamp NULL DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `digits` tinyint(3) unsigned NOT NULL DEFAULT '6',
  `seconds` tinyint(3) unsigned NOT NULL DEFAULT '30',
  `window` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `algorithm` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sha1',
  `recovery_codes` json DEFAULT NULL,
  `recovery_codes_generated_at` timestamp NULL DEFAULT NULL,
  `safe_devices` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `2fa_auth_type_auth_id_index` (`authenticatable_type`,`authenticatable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.two_factor_authentications : ~0 rows (environ)
/*!40000 ALTER TABLE `two_factor_authentications` DISABLE KEYS */;
/*!40000 ALTER TABLE `two_factor_authentications` ENABLE KEYS */;

-- Listage de la structure de la table laravel. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT '0',
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table laravel.users : ~2 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `type`, `name`, `email`, `email_verified_at`, `password`, `password_changed_at`, `active`, `timezone`, `last_login_at`, `last_login_ip`, `to_be_logged_out`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'admin', 'Super Admin', 'admin@admin.com', '2020-11-16 14:47:48', '$2y$10$FU6dlKhtSSC4zKYTfA0NTuWVQqzmOjLUrpMG140yXDugM99jb7xk.', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-11-16 14:47:48', '2020-11-16 14:47:48', NULL),
	(2, 'user', 'Test User', 'user@user.com', '2020-11-16 14:47:48', '$2y$10$BmMFPRoEI93g4u2F3o5MgO5LCts2PCf/ZyUc5vB1FJCOzHSbU3Uxu', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-11-16 14:47:48', '2020-11-16 14:47:48', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
