@extends('backend.layouts.app')

@section('title', __('Deactivated Products'))

@section('breadcrumb-links')
    @include('backend.shopping.product.includes.breadcrumb-links')
@endsection

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Deactivated Products')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.products-table status="deactivated" />
        </x-slot>
    </x-backend.card>
@endsection
