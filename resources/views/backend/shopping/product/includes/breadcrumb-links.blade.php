<x-utils.link
    class="c-subheader-nav-link"
    :href="route('admin.shopping.product.deactivated')"
    :text="__('Deactivated Products')"
    permission="admin.access.product.reactivate" />

@if ($logged_in_user->hasAllAccess())
    <x-utils.link class="c-subheader-nav-link" :href="route('admin.shopping.product.deleted')" :text="__('Deleted Products')" />
@endif
