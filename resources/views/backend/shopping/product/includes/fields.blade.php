<div>

    <div class="form-group row">
        <label for="image" class="col-md-2 col-form-label">@lang('Image')</label>

        <div class="col-md-10">
            <input type="url" name="image" class="form-control" placeholder="{{ __('Image Url') }}"
                   value="{{ old('image', $product->image) }}"  />
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

        <div class="col-md-10">
            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}"
                   value="{{ old('name', $product->name) }}" maxlength="100" required/>
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="detail" class="col-md-2 col-form-label">@lang('Detail')</label>

        <div class="col-md-10">
            <input type="text" name="detail" class="form-control" placeholder="{{ __('Detail') }}"
                   value="{{ old('detail', $product->detail) }}" maxlength="100" />
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="quantity" class="col-md-2 col-form-label">@lang('Quantity')</label>

        <div class="col-md-10">
            <input type="text" name="quantity" class="form-control" placeholder="{{ __('Quantity') }}"
                   value="{{ old('quantity', $product->quantity) }}" maxlength="100" required/>
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="price" class="col-md-2 col-form-label">@lang('Price')</label>

        <div class="col-md-10">
            <input type="number" name="price" class="form-control" placeholder="{{ __('Price') }}"
                   value="{{ old('price', $product->price) }}" required/>
        </div>
    </div><!--form-group-->

{{--    <div class="form-group row">--}}
{{--        <label for="price_unit" class="col-md-2 col-form-label">@lang('Price unit')</label>--}}

{{--        <div class="col-md-10">--}}
{{--            <input type="text" name="price_unit" class="form-control" placeholder="{{ __('Price unit') }}"--}}
{{--                   value="{{ old('price_unit', $product->price_unit) }}" maxlength="100" required/>--}}
{{--        </div>--}}
{{--    </div><!--form-group-->--}}


    <div class="form-group row">
        <label for="category_id" class="col-md-2 col-form-label">@lang('Price unit')</label>

        <div class="col-md-10">
            <select name="price_unit" class="form-control" required x-on:change="unit = $event.target.value">
                <option>-- select one --</option>
            @foreach(\App\Models\Product::getPriceUnits() as $unit)
                    <option value="{{ $unit }}" {{ $unit === $product->price_unit ? 'selected' : '' }}>{{ $unit }}</option>
                @endforeach
            </select>
        </div>
    </div><!--form-group-->


    <div class="form-group row">
        <label for="category_id" class="col-md-2 col-form-label">@lang('Category')</label>

        <div class="col-md-10">
            <select name="category_id" class="form-control" required x-on:change="categoryId = $event.target.value">
                <option>-- select one --</option>
                @foreach(\App\Models\Category::all() as $category)
                    <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="tags" class="col-md-2 col-form-label">@lang('Tags')</label>

        <div class="col-md-10">
            <input type="text" name="tags" class="form-control" placeholder="{{ __('Tags') }}"
                   value="{{ old('tags', $product->tags) }}" maxlength="100" />
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="active" class="col-md-2 col-form-label">@lang('Active')</label>

        <div class="col-md-10">
            <div class="form-check">
                <input name="active" id="active" class="form-check-input" type="checkbox"
                       value="1" {{ old('active', $product->enabled) ? 'checked' : '' }} />
            </div><!--form-check-->
        </div>
    </div><!--form-group-->


</div>
