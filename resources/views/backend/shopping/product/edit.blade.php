@inject('model', '\App\Models\Product')

@extends('backend.layouts.app')

@section('title', __('Update Product'))

@section('content')
    <x-forms.patch :action="route('admin.shopping.product.update', $product)">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Product')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.shopping.product.index')" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">

                @include('backend.shopping.product.includes.fields')

            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update Product')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
