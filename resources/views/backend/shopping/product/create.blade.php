@inject('model', '\App\Models\Product')
@inject('product', '\App\Models\Product')

@extends('backend.layouts.app')

@section('title', __('Create Product'))

@section('content')
    <x-forms.post :action="route('admin.shopping.product.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Product')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.shopping.product.index')" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">

                @include('backend.shopping.product.includes.fields')

            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create  Product')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection
