@extends('backend.layouts.app')

@section('title', __('View Product'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('View Product')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action" :href="route('admin.shopping.product.index')" :text="__('Back')" />
        </x-slot>

        <x-slot name="body">
            <table class="table table-hover">

                <tr>
                    <th>@lang('Image')</th>
                    <td><img src="{{ $product->image }}" class="user-profile-image" /></td>
                </tr>
                <tr>
                    <th>@lang('Name')</th>
                    <td>{{ $product->name }}</td>
                </tr>

                <tr>
                    <th>@lang('Slug')</th>
                    <td>{{ $product->slug }}</td>
                </tr>

                <tr>
                    <th>@lang('Uid')</th>
                    <td>{{ $product->uid }}</td>

                </tr>



                <tr>
                    <th>@lang('Quantity')</th>
                    <td>{{ $product->quantity }}</td>
                </tr>


                <tr>
                    <th>@lang('Price')</th>
                    <td>{{ $product->price }}</td>
                </tr>

                <tr>
                    <th>@lang('Price unit')</th>
                    <td>{{ $product->price_unit }}</td>
                </tr>

                <tr>
                    <th>@lang('Detail')</th>
                    <td>{{ $product->detail }}</td>
                </tr>

                <tr>
                    <th>@lang('Status')</th>
                    <td>@include('backend.shopping.product.includes.status', ['product' => $product])</td>
                </tr>


            </table>
        </x-slot>

        <x-slot name="footer">
            <small class="float-right text-muted">
                <strong>@lang('Created'):</strong> @displayDate($product->created_at) ({{ $product->created_at->diffForHumans() }}),
                <strong>@lang('Updated'):</strong> @displayDate($product->updated_at) ({{ $product->updated_at->diffForHumans() }})

                @if($product->trashed())
                    <strong>@lang('Deleted'):</strong> @displayDate($product->deleted_at) ({{ $product->deleted_at->diffForHumans() }})
                @endif
            </small>
        </x-slot>
    </x-backend.card>
@endsection
