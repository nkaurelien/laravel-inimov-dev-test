@inject('model', '\App\Models\Category')
@inject('category', '\App\Models\Product')

@extends('backend.layouts.app')

@section('title', __('Create Category'))

@section('content')
    <x-forms.post :action="route('admin.shopping.category.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Category')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.shopping.category.index')" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">

                @include('backend.shopping.category.includes.fields')

            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create  Category')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection
