@extends('backend.layouts.app')

@section('title', __('View Category'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('View Category')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action" :href="route('admin.shopping.category.index')" :text="__('Back')" />
        </x-slot>

        <x-slot name="body">
            <table class="table table-hover">

                <tr>
                    <th>@lang('Image')</th>
                    <td><img src="{{ $category->image }}" class="user-profile-image" /></td>
                </tr>
                <tr>
                    <th>@lang('Name')</th>
                    <td>{{ $category->name }}</td>
                </tr>

                <tr>
                    <th>@lang('Slug')</th>
                    <td>{{ $category->slug }}</td>
                </tr>

                <tr>
                    <th>@lang('Uid')</th>
                    <td>{{ $category->uid }}</td>

                </tr>


                <tr>
                    <th>@lang('Status')</th>
                    <td>@include('backend.shopping.category.includes.status', ['category' => $category])</td>
                </tr>


            </table>
        </x-slot>

        <x-slot name="footer">
            <small class="float-right text-muted">
                <strong>@lang('Created'):</strong> @displayDate($category->created_at) ({{ $category->created_at->diffForHumans() }}),
                <strong>@lang('Updated'):</strong> @displayDate($category->updated_at) ({{ $category->updated_at->diffForHumans() }})

                @if($category->trashed())
                    <strong>@lang('Deleted'):</strong> @displayDate($category->deleted_at) ({{ $category->deleted_at->diffForHumans() }})
                @endif
            </small>
        </x-slot>
    </x-backend.card>
@endsection
