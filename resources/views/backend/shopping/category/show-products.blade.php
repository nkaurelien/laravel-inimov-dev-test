@extends('backend.layouts.app')

@section('title', __('Category Management'))

@section('breadcrumb-links')
{{--    @include('backend.shopping.category.includes.breadcrumb-links')--}}
@endsection

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Category Product Management')
        </x-slot>


        <x-slot name="body">
            <livewire:backend.category-products-table categoryID='{{ $category->id }}' />
        </x-slot>
    </x-backend.card>
    <br>
    <x-backend.card>
        <x-slot name="header">
            @lang('Category Deactivated Product Management')
        </x-slot>


        <x-slot name="body">
            <livewire:backend.category-products-table categoryID='{{ $category->id }}' status="deactivated"/>
        </x-slot>
    </x-backend.card>
    <br>
    <x-backend.card>
        <x-slot name="header">
            @lang('Category Deleted Product Management')
        </x-slot>


        <x-slot name="body">
            <livewire:backend.category-products-table categoryID='{{ $category->id }}' status="deleted"/>
        </x-slot>
    </x-backend.card>
@endsection
