@if ($model->trashed())
    <x-utils.form-button
        :action="route('admin.shopping.category.restore', $model)"
        method="patch"
        button-class="btn btn-info btn-sm"
        icon="fas fa-sync-alt"
        name="confirm-item"
    >
        @lang('Restore')
    </x-utils.form-button>

    <x-utils.delete-button
        :href="route('admin.shopping.category.permanently-delete', $model)"
        :text="__('Permanently Delete')" />
@else
    <a class="btn btn-secondary btn-sm" href="{!! route('admin.shopping.category.show-products', $model) !!}">@lang('Show Products')</a>

    <x-utils.view-button :href="route('admin.shopping.category.show', $model)" />
    <x-utils.edit-button :href="route('admin.shopping.category.edit', $model)" />

{{--    @if (! $model->isActive())--}}
{{--        <x-utils.form-button--}}
{{--            :action="route('admin.shopping.category.mark', [$model, 1])"--}}
{{--            method="patch"--}}
{{--            button-class="btn btn-primary btn-sm"--}}
{{--            icon="fas fa-sync-alt"--}}
{{--            name="confirm-item"--}}
{{--            permission="admin.access.category.reactivate"--}}
{{--        >--}}
{{--            @lang('Reactivate')--}}
{{--        </x-utils.form-button>--}}
{{--    @endif--}}

    <x-utils.delete-button :href="route('admin.shopping.category.destroy', $model)" />

    <div class="dropdown d-inline-block">
        <a class="btn btn-sm btn-secondary dropdown-toggle" id="moreMenuLink" href="#" role="button" data-toggle="dropdown" data-boundary="window" aria-haspopup="true" aria-expanded="false">
            @lang('More')
        </a>

        <div class="dropdown-menu" aria-labelledby="moreMenuLink">


{{--            @if ($model->isActive())--}}

{{--            <x-utils.form-button--}}
{{--                :action="route('admin.shopping.category.mark', [$model, 0])"--}}
{{--                method="patch"--}}
{{--                name="confirm-item"--}}
{{--                button-class="dropdown-item"--}}
{{--                permission="admin.access.category.deactivate"--}}
{{--            >--}}
{{--                @lang('Deactivate')--}}
{{--            </x-utils.form-button>--}}
{{--            @endif--}}
        </div>
    </div>
@endif
