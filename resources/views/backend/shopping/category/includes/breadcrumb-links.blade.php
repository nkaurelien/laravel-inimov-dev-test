<x-utils.link
    class="c-subheader-nav-link"
    :href="route('admin.shopping.category.deactivated')"
    :text="__('Deactivated Categories')"
    permission="admin.access.category.reactivate" />

@if ($logged_in_user->hasAllAccess())
    <x-utils.link class="c-subheader-nav-link" :href="route('admin.shopping.category.deleted')" :text="__('Deleted Categories')" />
@endif
