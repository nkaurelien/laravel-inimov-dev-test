<div>
    <div class="form-group row">
        <label for="image" class="col-md-2 col-form-label">@lang('Image')</label>

        <div class="col-md-10">
            <input type="url" name="image" class="form-control" placeholder="{{ __('Image Url') }}"
                   value="{{ old('image', $category->image) }}"  />
        </div>
    </div><!--form-group-->

    <div class="form-group row">
        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

        <div class="col-md-10">
            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name', $category->name) }}" maxlength="100" required />
        </div>
    </div><!--form-group-->


    <div class="form-group row">
        <label for="active" class="col-md-2 col-form-label">@lang('Active')</label>

        <div class="col-md-10">
            <div class="form-check">
                <input name="active" id="active" class="form-check-input" type="checkbox" value="1" {{ old('active', $category->enabled) ? 'checked' : '' }} />
            </div><!--form-check-->
        </div>
    </div><!--form-group-->


</div>
