@inject('model', '\App\Models\Category')

@extends('backend.layouts.app')

@section('title', __('Update Category'))

@section('content')
    <x-forms.patch :action="route('admin.shopping.category.update', $category)">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Category')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.shopping.category.index')" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">

                @include('backend.shopping.category.includes.fields')

            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update Category')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
