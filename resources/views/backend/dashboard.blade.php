@extends('backend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>

        <x-slot name="body">
            @lang('Welcome to the Dashboard')
        </x-slot>
    </x-backend.card>

    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-12">
                    <x-backend.card>
                        <x-slot name="header">
                            @lang('Users Count')
                        </x-slot>

                        <x-slot name="body">
                            <h2>{{ \App\Domains\Auth\Models\User::count() }}</h2>
                        </x-slot>
                    </x-backend.card></div>
                <div class="col-12">
                    <x-backend.card>
                        <x-slot name="header">
                            @lang('Roles Count')
                        </x-slot>

                        <x-slot name="body">
                            <h2>{{ \App\Domains\Auth\Models\Role::count() }}</h2>
                        </x-slot>
                    </x-backend.card></div>
                <div class="col-12">
                    <x-backend.card>
                        <x-slot name="header">
                            @lang('Permissions Count')
                        </x-slot>

                        <x-slot name="body">
                            <h2>{{ \App\Domains\Auth\Models\Permission::count() }}</h2>
                        </x-slot>
                    </x-backend.card></div>
            </div>
        </div>
        <div class="col">

            <x-backend.card>
                <x-slot name="header">
                    @lang('Product Count')
                </x-slot>

                <x-slot name="body">
                    <h2>{{ \App\Models\Product::count() }}</h2>
                </x-slot>
            </x-backend.card>
        </div>
        <div class="col">

            <x-backend.card>
                <x-slot name="header">
                    @lang('Category Count')
                </x-slot>

                <x-slot name="body">
                    <h2>{{ \App\Models\Category::count() }}</h2>
                </x-slot>
            </x-backend.card>
        </div>
    </div>
@endsection
