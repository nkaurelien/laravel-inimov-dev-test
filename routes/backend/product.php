<?php

use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\DeactivatedProductController;
use App\Http\Controllers\Backend\DeletedProductController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;


// All route names are prefixed with 'admin.auth'.
Route::group([
    'prefix' => 'shopping',
    'as' => 'shopping.',
], function () {

    Route::redirect('/', '/admin/shopping/product', 301);

    Route::group([
        'prefix' => 'product',
        'as' => 'product.',
        'middleware' => 'role:' . config('boilerplate.access.role.admin'),
    ], function () {
        Route::get('deleted', [DeletedProductController::class, 'index'])
            ->name('deleted')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.product.index')
                    ->push(__('Deleted Products'), route('admin.shopping.product.deleted'));
            });

        Route::get('create', [ProductController::class, 'create'])
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.product.index')
                    ->push(__('Create Product'), route('admin.shopping.product.create'));
            });

        Route::post('/', [ProductController::class, 'store'])->name('store');

        Route::group(['prefix' => '{product}'], function () {
            Route::get('edit', [ProductController::class, 'edit'])
                ->name('edit')
                ->breadcrumbs(function (Trail $trail, Product $product) {
                    $trail->parent('admin.shopping.product.show', $product)
                        ->push(__('Edit'), route('admin.shopping.product.edit', $product));
                });

            Route::patch('/', [ProductController::class, 'update'])->name('update');
            Route::delete('/', [ProductController::class, 'destroy'])->name('destroy');
        });

        Route::group(['prefix' => '{deletedProduct}'], function () {
            Route::patch('restore', [DeletedProductController::class, 'update'])->name('restore');
            Route::delete('permanently-delete', [DeletedProductController::class, 'destroy'])->name('permanently-delete');
        });
    });

    Route::group([
        'prefix' => 'product',
        'as' => 'product.',
//        'middleware' => 'permission:admin.access.user.list|admin.access.user.deactivate|admin.access.user.reactivate',
    ], function () {
        Route::get('deactivated', [DeactivatedProductController::class, 'index'])
            ->name('deactivated')
//                ->middleware('permission:admin.access.user.reactivate')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.product.index')
                    ->push(__('Deactivated Products'), route('admin.shopping.product.deactivated'));
            });

        Route::get('/', [ProductController::class, 'index'])
            ->name('index')
//                ->middleware('permission:admin.access.user.list|admin.access.user.deactivate|admin.access.user.clear-session|admin.access.user.impersonate|admin.access.user.change-password')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.dashboard')
                    ->push(__('Product Management'), route('admin.shopping.product.index'));
            });

        Route::group(['prefix' => '{product}'], function () {
            Route::get('/', [ProductController::class, 'show'])
                ->name('show')
//                    ->middleware('permission:admin.access.user.list')
                ->breadcrumbs(function (Trail $trail, Product $product) {
                    $trail->parent('admin.shopping.product.index')
                        ->push($product->name, route('admin.shopping.product.show', $product));
                });

            Route::patch('mark/{status}', [DeactivatedProductController::class, 'update'])
                ->name('mark')
                ->where(['status' => '[0,1]']);
//                ->middleware('permission:admin.access.user.deactivate|admin.access.user.reactivate');

        });
    });

});
