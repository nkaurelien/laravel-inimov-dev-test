<?php

use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\DeactivatedCategoryController;
use App\Http\Controllers\Backend\DeletedCategoryController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;


// All route names are prefixed with 'admin.auth'.
Route::group([
    'prefix' => 'shopping',
    'as' => 'shopping.',
], function () {

    Route::redirect('/', '/admin/shopping/category', 301);

    Route::group([
        'prefix' => 'category',
        'as' => 'category.',
        'middleware' => 'role:' . config('boilerplate.access.role.admin'),
    ], function () {
        Route::get('deleted', [DeletedCategoryController::class, 'index'])
            ->name('deleted')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.category.index')
                    ->push(__('Deleted Categories'), route('admin.shopping.category.deleted'));
            });

        Route::get('create', [CategoryController::class, 'create'])
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.category.index')
                    ->push(__('Create Category'), route('admin.shopping.category.create'));
            });

        Route::post('/', [CategoryController::class, 'store'])->name('store');

        Route::group(['prefix' => '{category}'], function () {
            Route::get('edit', [CategoryController::class, 'edit'])
                ->name('edit')
                ->breadcrumbs(function (Trail $trail, Category $category) {
                    $trail->parent('admin.shopping.category.show', $category)
                        ->push(__('Edit'), route('admin.shopping.category.edit', $category));
                });

            Route::get('show-products', [CategoryController::class, 'showProducts'])
                ->name('show-products')
                ->breadcrumbs(function (Trail $trail, Category $category) {
                    $trail->parent('admin.shopping.category.show', $category)
                        ->push(__('Products'), route('admin.shopping.category.show-products', $category));
                });

            Route::patch('/', [CategoryController::class, 'update'])->name('update');
            Route::delete('/', [CategoryController::class, 'destroy'])->name('destroy');
        });

        Route::group(['prefix' => '{deletedCategory}'], function () {
            Route::patch('restore', [DeletedCategoryController::class, 'update'])->name('restore');
            Route::delete('permanently-delete', [DeletedCategoryController::class, 'destroy'])->name('permanently-delete');
        });
    });

    Route::group([
        'prefix' => 'category',
        'as' => 'category.',
//        'middleware' => 'permission:admin.access.user.list|admin.access.user.deactivate|admin.access.user.reactivate',
    ], function () {
        Route::get('deactivated', [DeactivatedCategoryController::class, 'index'])
            ->name('deactivated')
//                ->middleware('permission:admin.access.user.reactivate')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.shopping.category.index')
                    ->push(__('Deactivated Categories'), route('admin.shopping.category.deactivated'));
            });

        Route::get('/', [CategoryController::class, 'index'])
            ->name('index')
//                ->middleware('permission:admin.access.user.list|admin.access.user.deactivate|admin.access.user.clear-session|admin.access.user.impersonate|admin.access.user.change-password')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.dashboard')
                    ->push(__('Category Management'), route('admin.shopping.category.index'));
            });

        Route::group(['prefix' => '{category}'], function () {
            Route::get('/', [CategoryController::class, 'show'])
                ->name('show')
//                    ->middleware('permission:admin.access.user.list')
                ->breadcrumbs(function (Trail $trail, Category $category) {
                    $trail->parent('admin.shopping.category.index')
                        ->push($category->name, route('admin.shopping.category.show', $category));
                });

            Route::patch('mark/{status}', [DeactivatedCategoryController::class, 'update'])
                ->name('mark')
                ->where(['status' => '[0,1]']);
//                ->middleware('permission:admin.access.user.deactivate|admin.access.user.reactivate');

        });
    });

});
