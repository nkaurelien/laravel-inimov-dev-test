## Inimov Laravel Test App

This a simple test app for Laravel

Database sql and Database images assets (png, pdf) of ERD Diagram are located in ./storage dir.

### Demo Credentials


**Admin:** admin@admin.com  
**Password:** secret

**User:** user@user.com  
**Password:** secret


##### [Open Demo on Heroku Cloud](https://laravel-inimov-dev-test.herokuapp.com/)

App Navigation
- http://localhost:8000/admin/dashboard
- http://localhost:8000/account

### Features

manage **Users, roles and permissions**
manage **Shopping products with categories**


### Author

Hi, I'm Nkumbe Aurelien, Software enginneer, 72 years old


### Todo
- [ ] Fix heroku CI/CD (WIP)
- [ ] Upload and resize Product
- [ ] Improve ux/ui design and form with plugins


### Minimal ERD
![ERD Diagram](https://gitlab.com/nkaurelien/laravel-inimov-dev-test/-/raw/master/storage/Capture.PNG?inline=true)
<br> 
<br> 
<br>
##### Powered by
![Inimov Brand logo](https://www.inimov.com/images/logo-inimov-blanc.png)
