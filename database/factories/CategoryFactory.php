<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fake = $this->faker;

        $name = "Cat " . $fake->randomNumber(5);
        return [
            'uid' => Str::uuid(),
            'parent_id' => null,
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => $fake->imageUrl(500, 500),
            'enabled' => $fake->boolean(75),
            'created_at' => $fake->date('Y-m-d H:i:s'),
//        'updated_at' => $fake->date('Y-m-d H:i:s')
//        'deleted_at' => $fake->date('Y-m-d H:i:s'),
        ];
    }
}
