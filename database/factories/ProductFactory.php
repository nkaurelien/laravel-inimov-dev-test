<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fake = $this->faker;
        $name = "Art " . $fake->randomNumber(5);
        return [
            'uid' => Str::uuid(),
            'name' => $name,
            'slug' => Str::slug($name),
            'detail' => $fake->text(),
            'brand_name' => 'inimov',
            'tags' => 'tag1, tag2, tag3, tag4',
            'price' => $fake->randomNumber(3),
            'price_unit' => $fake->randomElement([Product::UNIT_XAF, Product::UNIT_EUR, Product::UNIT_USD]),
            'brand_logo' => 'https://www.inimov.com/images/logo-inimov-blanc.png',
            'image' => $fake->imageUrl(500, 500),
            'enabled' => $fake->boolean(75),
            'created_at' => $fake->date('Y-m-d H:i:s'),
        ];
    }
}
