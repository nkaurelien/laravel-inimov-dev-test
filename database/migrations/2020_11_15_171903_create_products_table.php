<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->nullable();
            $table->string('uid');
            $table->string('name');
            $table->string('slug');
            $table->string('detail')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('brand_logo')->nullable();
            $table->string('tags')->nullable();
            $table->integer('price')->default(1);
            $table->string('price_unit')->default(Product::UNIT_XAF);
            $table->integer('quantity')->default(1);
            $table->integer('type')->default(0);
            $table->text('image')->nullable();
            $table->text('calendar')->nullable();
            $table->boolean('enabled')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
