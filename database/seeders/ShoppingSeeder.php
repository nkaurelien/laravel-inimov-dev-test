<?php

namespace Database\Seeders;

use Database\Seeders\Shopping\CategorySeeder;
use Database\Seeders\Shopping\PermissionRoleSeeder;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;
use Illuminate\Database\Seeder;

/**
 * Class AuthTableSeeder.
 */
class ShoppingSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $this->truncateMultiple([
            'categories',
            'products',
        ]);

        $this->call(PermissionRoleSeeder::class);
        $this->call(CategorySeeder::class);

        $this->enableForeignKeys();
    }
}
