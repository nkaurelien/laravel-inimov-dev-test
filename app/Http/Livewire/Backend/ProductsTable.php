<?php

namespace App\Http\Livewire\Backend;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ProductsTable extends TableComponent
{

    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'name';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];
    /**
     * @var mixed
     */
    public $status;

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $query = Product::with('category')
            ->withCount('category');

        if ($this->status === 'deleted') {
            return $query->onlyTrashed();
        }

        if ($this->status === 'deactivated') {
            return $query->onlyDeactivated();
        }

        return $query->onlyActive();
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable()
                ->sortable(),
            Column::make(__('Price'), 'price')
                ->searchable()
                ->sortable()
                ->format(function (Product $model) {
                    return $model->price . ' ' . $model->price_unit;
                }),
            Column::make(__('Quantity'), 'quantity')
                ->searchable()
                ->sortable(),
            Column::make(__('Tags'), 'tags')
                ->sortable(),
            Column::make(__('category'), 'category.name')
                ->sortable(),
            Column::make(__('Actions'))
                ->format(function (Product $model) {
                    return view('backend.shopping.product.includes.actions', ['model' => $model]);
                }),
//            Column::make(__('Actions'))
//                ->format(function (Product $model) {
//                    return view('backend.auth.role.includes.actions', ['model' => $model]);
//                }),
        ];
    }


//    public function render()
//    {
//        return view('livewire.products-table');
//    }
}
