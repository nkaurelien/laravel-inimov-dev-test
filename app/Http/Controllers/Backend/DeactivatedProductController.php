<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Auth\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

/**
 * Class UserStatusController.
 */
class DeactivatedProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * DeactivatedUserController constructor.
     *
     * @param  ProductService  $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.shopping.product.deactivated');
    }

    /**
     * @param  Request  $request
     * @param  User  $user
     * @param $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(Request $request, Product $product, $status)
    {
        $this->productService->mark($product, (int) $status);

        return redirect()->route(
            (int) $status === 1 || ! $request->user()->can('admin.access.user.reactivate') ?
                'admin.shopping.product.index' :
                'admin.shopping.product.deactivated'
        )->withFlashSuccess(__('The product was successfully updated.'));
    }
}
