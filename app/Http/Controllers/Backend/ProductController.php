<?php

namespace App\Http\Controllers\Backend;

use App\Models\Product;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class UserController.
 */
class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.shopping.product.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.shopping.product.create');
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->name);
        $data['enabled'] = $request->has('active');
        $data['uid'] = Str::uuid();
        $user = $this->productService->store($data);

        return redirect()->route('admin.shopping.product.show', $user)->withFlashSuccess(__('The product was successfully created.'));
    }

    /**
     * @param  Product  $product
     *
     * @return mixed
     */
    public function show(Product $product)
    {
        return view('backend.shopping.product.show')
            ->withProduct($product);
    }

    /**
     * @param  Request  $request
     * @param  Product $product
     *
     * @return mixed
     */
    public function edit(Request $request, Product $product)
    {
        return view('backend.shopping.product.edit')
            ->withProduct($product);
    }

    /**
     * @param  Request  $request
     * @param  Product $product
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update(Request $request, Product $product)
    {
        $this->productService->update($product, $request->all());

        return redirect()->route('admin.shopping.product.show', $product)->withFlashSuccess(__('The product was successfully updated.'));
    }

    /**
     * @param  Request  $request
     * @param  Product $product
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Request $request, Product $product)
    {
        $this->productService->delete($product);

        return redirect()->route('admin.shopping.product.deleted')->withFlashSuccess(__('The product was successfully deleted.'));
    }
}
