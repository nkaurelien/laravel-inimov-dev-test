<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Auth\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\CategoryService;

/**
 * Class DeletedUserController.
 */
class DeletedCategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * DeletedUserController constructor.
     *
     * @param  CategoryService  $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.shopping.category.deleted');
    }

    /**
     * @param  Category  $deletedCategory
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(Category $deletedCategory)
    {

        $this->categoryService->restore($deletedCategory);

        return redirect()->route('admin.shopping.category.index')->withFlashSuccess(__('The category was successfully restored.'));
    }

    /**
     * @param  User  $deletedCategory
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Category $deletedCategory)
    {
        abort_unless(config('boilerplate.access.category.permanently_delete'), 404);

        $this->categoryService->destroy($deletedCategory);

        return redirect()->route('admin.shopping.category.deleted')->withFlashSuccess(__('The category was permanently deleted.'));
    }
}
