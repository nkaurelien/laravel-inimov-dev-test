<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Auth\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductService;

/**
 * Class DeletedUserController.
 */
class DeletedProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * DeletedUserController constructor.
     *
     * @param  ProductService  $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.shopping.product.deleted');
    }

    /**
     * @param  Product  $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(Product $deletedProduct)
    {

        $this->productService->restore($deletedProduct);

        return redirect()->route('admin.shopping.product.index')->withFlashSuccess(__('The product was successfully restored.'));
    }

    /**
     * @param  User  $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Product $deletedProduct)
    {
        abort_unless(config('boilerplate.access.product.permanently_delete'), 404);

        $this->productService->destroy($deletedProduct);

        return redirect()->route('admin.shopping.product.deleted')->withFlashSuccess(__('The product was permanently deleted.'));
    }
}
