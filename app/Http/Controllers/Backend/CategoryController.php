<?php

namespace App\Http\Controllers\Backend;

use App\Http\Livewire\Backend\CategoryProductsTable;
use App\Models\Category;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Livewire\Livewire;

/**
 * Class UserController.
 */
class CategoryController extends Controller
{

    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.shopping.category.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.shopping.category.create');
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->name);
        $data['enabled'] = $request->has('active');
        $data['uid'] = Str::uuid();
        $user = $this->categoryService->store($data);

        return redirect()->route('admin.shopping.category.show', $user)->withFlashSuccess(__('The category was successfully created.'));
    }

    /**
     * @param  Category  $category
     *
     * @return mixed
     */
    public function show(Category $category)
    {
        return view('backend.shopping.category.show')
            ->withCategory($category);
    }

    /**
     * @param  Category  $category
     *
     * @return mixed
     */
    public function showProducts(Category $category)
    {
        return view('backend.shopping.category.show-products')
            ->withCategory($category)
            ->withProducts($category->products);
    }

    /**
     * @param  Request  $request
     * @param  Category $category
     *
     * @return mixed
     */
    public function edit(Request $request, Category $category)
    {
        return view('backend.shopping.category.edit')
            ->withCategory($category);
    }

    /**
     * @param  Request  $request
     * @param  Category $category
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update(Request $request, Category $category)
    {
        $this->categoryService->update($category, $request->all());

        return redirect()->route('admin.shopping.category.show', $category)->withFlashSuccess(__('The category was successfully updated.'));
    }

    /**
     * @param  Request  $request
     * @param  Category $category
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Request $request, Category $category)
    {
        $this->categoryService->delete($category);

        return redirect()->route('admin.shopping.category.deleted')->withFlashSuccess(__('The category was successfully deleted.'));
    }
}
