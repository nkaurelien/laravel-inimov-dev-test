<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use HasFactory,
        SoftDeletes,
        LogsActivity;

    protected $fillable = [
        'uid',
        'slug',
        'image',
        'name',
        'enabled',
    ];


    /**
     * @var string[]
     */
    protected $casts = [
        'enabled' => 'boolean',
    ];

    public static $rules = [
        'name' => 'string|required|min:3',
        'image' => 'nullable|string',
    ];
    public function products (): HasMany
    {
        return $this->hasMany(Product::class);
    }


    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->enabled;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyDeactivated($query)
    {
        return $query->whereEnabled(false);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyActive($query)
    {
        return $query->whereEnabled(true);
    }

    /**
     * @param $query
     * @param $type
     *
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }
}
