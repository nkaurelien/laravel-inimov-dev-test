<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use HasFactory,
        SoftDeletes,
        LogsActivity;

    const UNIT_XAF = 'XAF';
    const UNIT_EUR = 'EUR';
    const UNIT_USD = 'USD';

    protected $fillable = [
        'name',
        'uid',
        'slug',
        'detail',
        'price',
        'price_unit',
        'enabled',
    ];


    /**
     * @var string[]
     */
    protected $casts = [
        'enabled' => 'boolean',
    ];


    public static $rules = [
        'name' => 'string|required|min:3',
        'detail'=> 'string|nullable|min:3',
        'brand_name' => 'string|nullable',
        'brand_logo' => 'string|nullable',
        'tags' => 'string|nullable',
        'image' => 'nullable|string',
        'calendar' => 'nullable|string',
    ];

    public function category (): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->enabled;
    }


    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyDeactivated($query)
    {
        return $query->whereEnabled(false);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyActive($query)
    {
        return $query->whereEnabled(true);
    }

    /**
     * @param $query
     * @param $type
     *
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

    public static function getPriceUnits()
    {
        return [
            Product::UNIT_XAF => Product::UNIT_XAF,
            Product::UNIT_USD => Product::UNIT_USD,
            Product::UNIT_EUR => Product::UNIT_EUR,
        ];
    }

}
