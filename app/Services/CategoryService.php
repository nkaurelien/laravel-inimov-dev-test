<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\Category;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class UserService.
 */
class CategoryService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param  Category  $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * @param  array  $data
     *
     * @return Category
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Category
    {
        DB::beginTransaction();

        try {
            $model = $this->model::create($data);

        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating this category. Please try again.'));
        }

        DB::commit();


        return $model;
    }

    /**
     * @param  Category  $category
     * @param  array  $data
     *
     * @return Category
     * @throws \Throwable
     */
    public function update(Category $category, array $data = []): Category
    {
        DB::beginTransaction();

        try {
            $category->update($data);


        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating this category. Please try again.'));
        }

        DB::commit();

        return $category;
    }




    /**
     * @param  Category  $category
     * @param $status
     *
     * @return Category
     * @throws GeneralException
     */
    public function mark(Category $category, $status): Category
    {

        $category->enabled = $status;

        if ($category->save()) {

            return $category;
        }

        throw new GeneralException(__('There was a problem updating this category. Please try again.'));
    }

    /**
     * @param  Category  $category
     *
     * @return Category
     * @throws GeneralException
     */
    public function delete(Category $category): Category
    {

        if ($this->deleteById($category->id)) {

            return $category;
        }

        throw new GeneralException('There was a problem deleting this category. Please try again.');
    }

    /**
     * @param Category $category
     *
     * @return Category
     *@throws GeneralException
     */
    public function restore(Category $category): Category
    {
        if ($category->restore()) {

            return $category;
        }

        throw new GeneralException(__('There was a problem restoring this category. Please try again.'));
    }

    /**
     * @param  Category $category
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Category $category): bool
    {
        if ($category->forceDelete()) {

            return true;
        }

        throw new GeneralException(__('There was a problem permanently deleting this category. Please try again.'));
    }

}
