<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class UserService.
 */
class ProductService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param  Product  $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /**
     * @param  array  $data
     *
     * @return Product
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Product
    {
        DB::beginTransaction();

        try {
            $model = $this->model::create($data);

        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating this product. Please try again.'));
        }

        DB::commit();


        return $model;
    }

    /**
     * @param  Product  $item
     * @param  array  $data
     *
     * @return Product
     * @throws \Throwable
     */
    public function update(Product $item, array $data = []): Product
    {
        DB::beginTransaction();

        try {
            $item->update($data);


        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating this product. Please try again.'));
        }

        DB::commit();

        return $item;
    }




    /**
     * @param  Product  $item
     * @param $status
     *
     * @return Product
     * @throws GeneralException
     */
    public function mark(Product $item, $status): Product
    {

        $item->enabled = $status;

        if ($item->save()) {

            return $item;
        }

        throw new GeneralException(__('There was a problem updating this product. Please try again.'));
    }

    /**
     * @param  Product  $product
     *
     * @return Product
     * @throws GeneralException
     */
    public function delete(Product $product): Product
    {

        if ($this->deleteById($product->id)) {

            return $product;
        }

        throw new GeneralException('There was a problem deleting this product. Please try again.');
    }

    /**
     * @param Product $product
     *
     * @return Product
     *@throws GeneralException
     */
    public function restore(Product $product): Product
    {
        if ($product->restore()) {

            return $product;
        }

        throw new GeneralException(__('There was a problem restoring this product. Please try again.'));
    }

    /**
     * @param  Product $product
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Product $product): bool
    {
        if ($product->forceDelete()) {

            return true;
        }

        throw new GeneralException(__('There was a problem permanently deleting this product. Please try again.'));
    }

}
